package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.entity.Account;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MoneyTransferServiceTest {

    @Test
    public void transferShouldDecrementFromAccountAndIncrementToAccount() {
        // GIVEN
        final Integer transferValue = 18;
        final Integer fromAccountAmount = 456;
        final Integer toAccountAmount = 101;
        final Account fromAccount = new Account("033", "8478-9", new BigDecimal(fromAccountAmount));
        final Account toAccount = new Account("033", "8492-5", new BigDecimal(toAccountAmount));

        Integer expectedFromAccountAmount = fromAccountAmount - transferValue;
        Integer expectedToAccountAmount = toAccountAmount + transferValue;

        // WHEN
        fromAccount.transferTo(toAccount, new BigDecimal(transferValue));

        // THEN
        assertEquals("fromAccount amount", new BigDecimal(expectedFromAccountAmount), fromAccount.getAmount());
        assertEquals("toAccount amount", new BigDecimal(expectedToAccountAmount), toAccount.getAmount());
    }
}
