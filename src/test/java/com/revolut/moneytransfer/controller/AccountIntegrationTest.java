package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.ApplicationStartUp;
import com.revolut.moneytransfer.dto.AccountDTO;
import com.revolut.moneytransfer.entity.Account;
import com.revolut.moneytransfer.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = ApplicationStartUp.class, webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class AccountIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AccountRepository repository;

    @Before
    public void setUp() {
        repository.getAccounts().clear();
    }

    @Test
    public void shouldReturnOkAndAllAccounts() {
        // GIVEN
        repository.createAccount(new Account("055", "1256-9", BigDecimal.ZERO));
        repository.createAccount(new Account("055", "2322-9", BigDecimal.ONE));
        Integer expectedRegistries = repository.getAccounts().size();

        // WHEN
        final ResponseEntity<Collection> response =
            restTemplate.getForEntity("/accounts", Collection.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasSize(expectedRegistries);
    }

    @Test
    public void shouldReturnOKAndEmptyDtoListBodyWhenNoAccountFound() {
        // GIVEN no account registered

        // WHEN
        final ResponseEntity<Collection> response =
            restTemplate.getForEntity("/accounts", Collection.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEmpty();
    }

    @Test
    public void shouldReturnOkAndAccount() {
        // GIVEN
        String bankNumber = "033";
        String accountNumber = "01012-3";

        // GIVEN registered account
        repository.createAccount(new Account(bankNumber, accountNumber, BigDecimal.ZERO));

        // WHEN
        final ResponseEntity<AccountDTO> response =
            restTemplate.getForEntity("/accounts/" + accountNumber + "/bank/" + bankNumber,
                AccountDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasFieldOrPropertyWithValue("number", accountNumber);
        assertThat(response.getBody()).hasFieldOrPropertyWithValue("bank", bankNumber);
    }

    @Test
    public void shouldReturnOkAndEmptyDtoBodyWhenNoAccountFound() {
        // GIVEN
        String bankNumber = "0xx";
        String accountNumber = "0000-3";

        // WHEN
        final ResponseEntity<AccountDTO> response =
            restTemplate.getForEntity("/accounts/" + accountNumber + "/bank/" + bankNumber,
                AccountDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(new AccountDTO());
    }

    @Test
    public void shouldReturnCreatedWhenPostingAccount() {
        // GIVEN
        final BigDecimal amount = BigDecimal.ZERO;
        final String accountNumber = "01012-3";
        final String bankNumber = "033";
        final AccountDTO requestDto = new AccountDTO(bankNumber, accountNumber, amount);

        // WHEN
        final ResponseEntity<Void> response =
            restTemplate.postForEntity("/accounts", requestDto, Void.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void shouldReturnConflictWhenPostingRegisteredAccount() {
        // GIVEN
        final BigDecimal amount = BigDecimal.ZERO;
        final String accountNumber = "01012-3";
        final String bankNumber = "033";
        final AccountDTO requestDto = new AccountDTO(bankNumber, accountNumber, amount);

        // GIVEN registered account
        repository.createAccount(new Account(bankNumber, accountNumber, BigDecimal.ZERO));

        // WHEN
        final ResponseEntity<Void> response =
            restTemplate.postForEntity("/accounts", requestDto, Void.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    public void shouldReturnBadRequestWhenPostingEmptyObject() {
        // GIVEN
        final AccountDTO accountDto = new AccountDTO();

        // WHEN
        final ResponseEntity<Void> response =
            restTemplate.postForEntity("/accounts", accountDto, Void.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}
