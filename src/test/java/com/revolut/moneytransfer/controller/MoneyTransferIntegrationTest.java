package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.ApplicationStartUp;
import com.revolut.moneytransfer.dto.MoneyTransferDTO;
import com.revolut.moneytransfer.entity.Account;
import com.revolut.moneytransfer.entity.MoneyTransfer;
import com.revolut.moneytransfer.repository.AccountRepository;
import com.revolut.moneytransfer.repository.MoneyTransferRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = ApplicationStartUp.class, webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class MoneyTransferIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MoneyTransferRepository moneyTransferRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setUp() {
        accountRepository.getAccounts().clear();
        moneyTransferRepository.getMoneyTransfers().clear();
    }

    @Test
    public void shouldReturnOkAndAllMoneyTransfers() {
        // GIVEN
        final String transfer1Id = UUID.randomUUID().toString();
        final String transfer2Id = UUID.randomUUID().toString();
        final BigDecimal tenAmount = BigDecimal.TEN;
        final BigDecimal oneAmount = BigDecimal.ONE;
        final String accountFrom = "8922-4";
        final String accountTo = "4903-0";
        String bank = "076";

        // GIVEN registered money transfers
        final List<MoneyTransfer> moneyTransfers = new ArrayList<>();
        moneyTransfers.add(new MoneyTransfer(transfer1Id, accountFrom, bank, accountTo, bank, tenAmount,
            LocalDateTime.now().minusHours(3)));
        moneyTransfers.add(new MoneyTransfer(transfer2Id, accountFrom, bank, accountTo, bank, oneAmount,
            LocalDateTime.now().minusHours(4)));
        moneyTransferRepository.setMoneyTransfers(moneyTransfers);

        // GIVEN expected JSON
        Map<String, Object> expectedMoneyTransfer1 = createMoneyTransferJson(transfer1Id, accountFrom, accountTo, tenAmount);
        Map<String, Object> expectedMoneyTransfer2 = createMoneyTransferJson(transfer2Id, accountFrom, accountTo, oneAmount);
        List<Map<String, Object>> expectedJson = new ArrayList<>();
        expectedJson.add(expectedMoneyTransfer1);
        expectedJson.add(expectedMoneyTransfer2);

        // WHEN
        final ResponseEntity<Collection> response =
            restTemplate.getForEntity("/transfers", Collection.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasSameSizeAs(expectedJson);
    }

    @Test
    public void shouldReturnOKAndEmptyDtoListBodyWhenNoMoneyTransferFound() {
        // GIVEN no money transfer registered

        // WHEN
        final ResponseEntity<Collection> response =
            restTemplate.getForEntity("/transfers", Collection.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEmpty();
    }

    @Test
    public void shouldReturnOkAndEmptyDtoBodyWhenNoMoneyTransferFound() {
        // GIVEN
        String transferId = UUID.randomUUID().toString();

        // WHEN
        final ResponseEntity<MoneyTransferDTO> response =
            restTemplate.getForEntity("/transfers/" + transferId, MoneyTransferDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(new MoneyTransferDTO());
    }

    @Test
    public void shouldReturnOkAndMoneyTransfer() {
        // GIVEN
        String transferId = UUID.randomUUID().toString();
        final String accountFrom = "0455-9";
        final String accountTo = "8493-0";
        final BigDecimal amount = BigDecimal.TEN;
        final String bank = "078";
        final LocalDateTime transferDate = LocalDateTime.now().minusHours(1);

        // GIVEN registered money transfers
        final List<MoneyTransfer> moneyTransfers = new ArrayList<>();
        moneyTransfers.add(new MoneyTransfer(transferId, accountFrom, bank, accountTo, bank, amount,
            transferDate));
        moneyTransferRepository.setMoneyTransfers(moneyTransfers);

        // WHEN
        final ResponseEntity<MoneyTransferDTO> response =
            restTemplate.getForEntity("/transfers/" + transferId, MoneyTransferDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(new MoneyTransferDTO(transferId, bank, accountFrom,
            bank, accountTo, amount, transferDate.toString()));
    }

    @Test
    public void shouldReturnSuccessOnMoneyTransferExecution() {
        // GIVEN
        final String bank = "033";
        final String accountFromNumber = "01012-3";
        final String accontToNumber = "01234-9";
        final BigDecimal transferValue = new BigDecimal(100);
        final MoneyTransferDTO moneyTransferDTO = new MoneyTransferDTO(bank, accountFromNumber,
            bank, accontToNumber, transferValue);

        // GIVEN registered accounts
        accountRepository.createAccount(new Account(bank, accountFromNumber, new BigDecimal(1500)));
        accountRepository.createAccount(new Account(bank, accontToNumber, new BigDecimal(1000)));

        // WHEN
        final ResponseEntity<MoneyTransferDTO> response =
            restTemplate.postForEntity("/transfers", moneyTransferDTO, MoneyTransferDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasFieldOrProperty("id").isNotNull();
        assertThat(response.getBody()).hasFieldOrProperty("transferDate").isNotNull();
    }

    @Test
    public void shouldReturnNotFoundWhenInvalidAccountsOnMoneyTransferExecution() {
        // GIVEN
        final String bank = "033";
        final String accountFromNumber = "01012-3";
        final String accontToNumber = "01234-9";
        final BigDecimal transferValue = new BigDecimal(100);
        final MoneyTransferDTO moneyTransferDTO = new MoneyTransferDTO(bank, accountFromNumber,
            bank, accontToNumber, transferValue);

        // GIVEN only one registered account
        accountRepository.createAccount(new Account(bank, accountFromNumber, new BigDecimal(1500)));

        // WHEN
        final ResponseEntity<MoneyTransferDTO> response =
            restTemplate.postForEntity("/transfers", moneyTransferDTO, MoneyTransferDTO.class);

        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private Map<String,Object> createMoneyTransferJson(String transfer1Id, String accountFrom, String accountTo,
        BigDecimal tenAmount) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", transfer1Id);
        map.put("accountFrom", accountFrom);
        map.put("accountTo", accountTo);
        map.put("value", tenAmount);
        return map;
    }
}
