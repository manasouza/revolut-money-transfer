package com.revolut.moneytransfer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO {

    @NotEmpty
    private String bank;
    @NotEmpty
    private String number;
    @NotNull
    private BigDecimal amount;

    /*
     * Constructor needed for org.springframework.web.bind.annotation.ResponseBody type definition purposes
     */
    public AccountDTO() {
    }

    public AccountDTO(String bank, String number) {
        this.bank = bank;
        this.number = number;
    }

    public AccountDTO(String bank, String number, BigDecimal amount) {
        this.bank = bank;
        this.number = number;
        this.amount = amount;
    }

    public String getBank() {
        return bank;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AccountDTO that = (AccountDTO) o;
        return Objects.equals(getBank(), that.getBank()) &&
            Objects.equals(getNumber(), that.getNumber()) &&
            Objects.equals(getAmount(), that.getAmount());
    }
}
