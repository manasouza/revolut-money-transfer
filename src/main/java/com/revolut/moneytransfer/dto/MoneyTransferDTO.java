package com.revolut.moneytransfer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoneyTransferDTO {

    private String id;
    @NotEmpty
    private String bankFrom;
    @NotEmpty
    private String accountFrom;
    @NotEmpty
    private String bankTo;
    @NotEmpty
    private String accountTo;
    @NotNull
    @Min(1)
    private BigDecimal value;
    private String transferDate;

    /*
     * Constructor needed for org.springframework.web.bind.annotation.ResponseBody type definition purposes
     */
    public MoneyTransferDTO() {
    }

    public MoneyTransferDTO(String bankFrom, String accountFrom, String bankTo,
        String accountTo, BigDecimal value) {
        this.bankFrom = bankFrom;
        this.accountFrom = accountFrom;
        this.bankTo = bankTo;
        this.accountTo = accountTo;
        this.value = value;
    }

    public MoneyTransferDTO(String id, String bankFrom, String accountFrom, String bankTo, String accountTo,
        BigDecimal value, String transferDate) {
        this(bankFrom, accountFrom, bankTo, accountTo, value);
        this.id = id;
        this.transferDate = transferDate;
    }

    public String getId() {
        return id;
    }

    public String getAccountFrom() {
        return accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getBankFrom() {
        return bankFrom;
    }

    public String getBankTo() {
        return bankTo;
    }

    public String getTransferDate() {
        return transferDate;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
               return true;
        if (o == null || getClass() != o.getClass())
            return false;
        MoneyTransferDTO that = (MoneyTransferDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(bankFrom, that.bankFrom) &&
            Objects.equals(getAccountFrom(), that.getAccountFrom()) &&
            Objects.equals(bankTo, that.bankTo) &&
            Objects.equals(getAccountTo(), that.getAccountTo()) &&
            Objects.equals(getValue(), that.getValue());
    }

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder("MoneyTransferDTO{");
        sb.append("id='").append(id).append('\'');
        sb.append(", bankFrom='").append(bankFrom).append('\'');
        sb.append(", accountFrom='").append(accountFrom).append('\'');
        sb.append(", bankTo='").append(bankTo).append('\'');
        sb.append(", accountTo='").append(accountTo).append('\'');
        sb.append(", value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}
