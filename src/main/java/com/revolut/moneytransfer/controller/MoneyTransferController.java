package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.dto.MoneyTransferDTO;
import com.revolut.moneytransfer.repository.MoneyTransferRepository;
import com.revolut.moneytransfer.service.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class MoneyTransferController {

    @Autowired
    private MoneyTransferRepository repository;

    @Autowired
    private MoneyTransferService service;

    private final DtoValidator<MoneyTransferDTO> dtoValidator;

    public MoneyTransferController() {
        this.dtoValidator = new DtoValidator<>();
    }

    @GetMapping("/transfers/{id}")
    public MoneyTransferDTO getMoneyTransfer(@PathVariable String id) {
        return DtoEntityParser.parseEntityToDto(repository.getMoneyTransferById(id));
    }

    @GetMapping("/transfers")
    public Collection<MoneyTransferDTO> getMoneyTransfers() {
        return DtoEntityParser.parseMoneyTransferEntityToDto(repository.getMoneyTransfers());
    }

    @PostMapping("/transfers")
    public MoneyTransferDTO executeMoneyTransfer(@RequestBody MoneyTransferDTO dto) {
        dtoValidator.validate(dto);
        return DtoEntityParser.parseEntityToDto(service.transferValue(dto.getAccountFrom(), dto.getBankFrom(), dto.getAccountTo(),
            dto.getBankTo(), dto.getValue()));
    }
}
