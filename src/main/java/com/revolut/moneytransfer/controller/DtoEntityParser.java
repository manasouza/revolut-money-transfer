package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.dto.AccountDTO;
import com.revolut.moneytransfer.dto.MoneyTransferDTO;
import com.revolut.moneytransfer.entity.Account;
import com.revolut.moneytransfer.entity.MoneyTransfer;

import java.util.Collection;
import java.util.stream.Collectors;

class DtoEntityParser {



    static Account parseDtoToEntity(AccountDTO dto) {
        return new Account(dto.getBank(), dto.getNumber(), dto.getAmount());
    }

    static Collection<AccountDTO> parseAccountEntityToDto(Collection<Account> accounts) {
        return accounts.stream()
            .map(account -> new AccountDTO(account.getBank(),
                account.getNumber(), account.getAmount()))
            .collect(Collectors.toSet());
    }

    static MoneyTransferDTO parseEntityToDto(MoneyTransfer moneyTransfer) {
        final String transferDate = moneyTransfer.getTransferDate() == null
            ? null : moneyTransfer.getTransferDate().toString();
        return new MoneyTransferDTO(moneyTransfer.getId(),
            moneyTransfer.getBankFrom(), moneyTransfer.getAccountFrom(),
            moneyTransfer.getBankTo(), moneyTransfer.getAccountTo(),
            moneyTransfer.getValue(), transferDate);
    }

    static Collection<MoneyTransferDTO> parseMoneyTransferEntityToDto(
        Collection<MoneyTransfer> moneyTransfers) {
        return moneyTransfers.stream()
            .map(moneyTransfer -> new MoneyTransferDTO(moneyTransfer.getId(),
                moneyTransfer.getBankFrom(), moneyTransfer.getAccountFrom(),
                moneyTransfer.getBankTo(), moneyTransfer.getAccountTo(),
                moneyTransfer.getValue(),
                moneyTransfer.getTransferDate() == null ? null : moneyTransfer.getTransferDate().toString()))
            .collect(Collectors.toList());
    }

    static AccountDTO parseEntityToDto(Account account) {
        return new AccountDTO(account.getBank(), account.getNumber(), account.getAmount());
    }

}
