package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.dto.AccountDTO;
import com.revolut.moneytransfer.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class AccountController {

    @Autowired
    private AccountRepository repository;

    private DtoValidator<AccountDTO> dtoValidator;

    public AccountController() {
        dtoValidator = new DtoValidator<>();
    }

    @GetMapping("/accounts/{accountNumber}/bank/{bankNumber}")
    public AccountDTO getAccount(@PathVariable String accountNumber,
        @PathVariable String bankNumber) {
        return DtoEntityParser.parseEntityToDto(
            repository.getAccount(
                DtoEntityParser.parseDtoToEntity(new AccountDTO(bankNumber, accountNumber))));
    }

    @GetMapping("/accounts")
    public Collection<AccountDTO> getAccounts() {
        return DtoEntityParser.parseAccountEntityToDto(repository.getAccounts());
    }

    @PostMapping("/accounts")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addAccount(@RequestBody AccountDTO dto) {
        dtoValidator.validate(dto);
        repository.createAccount(DtoEntityParser.parseDtoToEntity(dto));
    }

}
