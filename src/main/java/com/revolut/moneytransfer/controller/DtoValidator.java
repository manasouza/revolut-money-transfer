package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.exception.RequiredPropertyException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class DtoValidator<V> {

    private Validator validator;

    DtoValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    void validate(V dtoObject) {
        List<String> messageList = new ArrayList<>();
        final Set<ConstraintViolation<V>> validate = validator.validate(dtoObject);
        if (!validate.isEmpty()) {
            for (ConstraintViolation<V> violation : validate) {
                messageList.add(String.format("Property %s %s", violation.getPropertyPath(), violation.getMessage()));
            }
            throw new RequiredPropertyException(messageList.toString());
        }
    }
}
