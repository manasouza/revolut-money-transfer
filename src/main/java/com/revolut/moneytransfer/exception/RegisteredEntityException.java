package com.revolut.moneytransfer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class RegisteredEntityException extends RuntimeException {

    public RegisteredEntityException() {
        super("Entity already registered");
    }
}
