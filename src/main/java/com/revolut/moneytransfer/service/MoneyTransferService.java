package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.entity.Account;
import com.revolut.moneytransfer.entity.MoneyTransfer;
import com.revolut.moneytransfer.exception.ResourceNotFoundException;
import com.revolut.moneytransfer.repository.AccountRepository;
import com.revolut.moneytransfer.repository.MoneyTransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class MoneyTransferService {

    public static final String MISSING_ACCOUNT_MESSAGE =
        "Missing registered information from bank: %s and account number: %s";
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private MoneyTransferRepository moneyTransferRepository;

    public MoneyTransfer transferValue(String accountNumberFrom, String bankFrom, String accountNumberTo, String bankTo,
        BigDecimal value) {
        final Account fromAccount = accountRepository.getAccount(new Account(bankFrom, accountNumberFrom));
        final Account toAccount = accountRepository.getAccount(new Account(bankTo, accountNumberTo));
        validateAccountExists(accountNumberFrom, bankFrom, accountNumberTo, bankTo, fromAccount,
            toAccount);
        fromAccount.transferTo(toAccount, value);
        final MoneyTransfer transfer =
            moneyTransferRepository.createTransfer(new MoneyTransfer(fromAccount, toAccount, value));
        return transfer;
    }

    private void validateAccountExists(String accountNumberFrom, String bankFrom,
        String accountNumberTo, String bankTo, Account fromAccount, Account toAccount) {
        if (fromAccount.isEmpty()) {
            throw new ResourceNotFoundException(String.format(MISSING_ACCOUNT_MESSAGE,
                bankFrom, accountNumberFrom));
        }
        if (toAccount.isEmpty()) {
            throw new ResourceNotFoundException(String.format(MISSING_ACCOUNT_MESSAGE,
                bankTo, accountNumberTo));
        }
    }
}
