package com.revolut.moneytransfer.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {

    private String bank;
    private String number;
    private BigDecimal amount;

    public Account() {
    }

    public Account(String bank, String number, BigDecimal amount) {
        this.bank = bank;
        this.number = number;
        this.amount = amount;
    }

    public Account(String bank, String number) {
        this.bank = bank;
        this.number = number;
    }

    public String getBank() {
        return bank;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Account account = (Account) o;
        return Objects.equals(getBank(), account.getBank()) &&
            Objects.equals(getNumber(), account.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBank(), getNumber());
    }

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder("Account{");
        sb.append("bank='").append(bank).append('\'');
        sb.append(", number='").append(number).append('\'');
        sb.append(", amount=").append(amount);
        sb.append('}');
        return sb.toString();
    }

    public void transferTo(Account toAccount, BigDecimal transferValue) {
        this.setAmount(this.getAmount().subtract(transferValue));
        toAccount.setAmount(toAccount.getAmount().add(transferValue));
    }

    public boolean isEmpty() {
        return this.getAmount() == null
        && getNumber() == null
        && getBank() == null;
    }
}
