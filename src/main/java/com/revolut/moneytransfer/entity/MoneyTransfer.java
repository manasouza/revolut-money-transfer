package com.revolut.moneytransfer.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MoneyTransfer {

    private String id;
    private String accountFrom;
    private String bankFrom;
    private String accountTo;
    private String bankTo;
    private BigDecimal value;
    private LocalDateTime transferDate;

    public MoneyTransfer() {
    }

    public MoneyTransfer(String accountFrom, String bankFrom, String accountTo, String bankTo,
        BigDecimal value) {
        this.accountFrom = accountFrom;
        this.bankFrom = bankFrom;
        this.accountTo = accountTo;
        this.bankTo = bankTo;
        this.value = value;
    }

    public MoneyTransfer(String id, String accountFrom, String bankFrom, String accountTo,
        String bankTo, BigDecimal value, LocalDateTime transferDate) {
        this(accountFrom, bankFrom, accountTo, bankTo, value);
        this.id = id;
        this.transferDate = transferDate;
    }

    public MoneyTransfer(Account fromAccount, Account toAccount, BigDecimal value) {
        this.accountFrom = fromAccount.getNumber();
        this.bankFrom = fromAccount.getBank();
        this.accountTo = toAccount.getNumber();
        this.bankTo = toAccount.getBank();
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getAccountFrom() {
        return accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getBankFrom() {
        return bankFrom;
    }

    public String getBankTo() {
        return bankTo;
    }

    public LocalDateTime getTransferDate() {
        return transferDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTransferDate(LocalDateTime transferDate) {
        this.transferDate = transferDate;
    }
}
