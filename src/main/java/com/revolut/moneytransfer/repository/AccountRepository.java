package com.revolut.moneytransfer.repository;

import com.revolut.moneytransfer.entity.Account;
import com.revolut.moneytransfer.exception.RegisteredEntityException;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;

@Repository
public class AccountRepository {

    private Collection<Account> accounts;

    AccountRepository() {
        this.accounts = new HashSet<>();
    }

    public void createAccount(Account account) {
        if (accounts.contains(account)) {
            throw new RegisteredEntityException();
        }
        this.accounts.add(account);
    }

    public Collection<Account> getAccounts() {
        return this.accounts;
    }

    public Account getAccount(Account account) {
        return this.accounts.stream()
            .filter(acc -> acc.equals(account))
            .findFirst()
            .orElse(new Account());
    }
}
