package com.revolut.moneytransfer.repository;

import com.revolut.moneytransfer.entity.MoneyTransfer;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Repository
public class MoneyTransferRepository {

    private Collection<MoneyTransfer> moneyTransfers;

    MoneyTransferRepository() {
        moneyTransfers = new ArrayList<>();
    }

    public MoneyTransfer getMoneyTransferById(String moneyTransferId) {
        return moneyTransfers.stream()
            .filter(m -> moneyTransferId.equals(m.getId()))
            .findFirst()
            .orElse(new MoneyTransfer());
    }

    public Collection<MoneyTransfer> getMoneyTransfers() {
        return moneyTransfers;
    }

    public void setMoneyTransfers(List<MoneyTransfer> moneyTransfers) {
        this.moneyTransfers = moneyTransfers;
    }

    public MoneyTransfer createTransfer(MoneyTransfer moneyTransfer) {
        moneyTransfer.setId(UUID.randomUUID().toString());
        moneyTransfer.setTransferDate(LocalDateTime.now());
        moneyTransfers.add(moneyTransfer);
        return moneyTransfer;
    }
}
